﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

/* 
 * Aplikacja dodatkowo po wskazaniu procesu/procesow powinna zapewnic ciaglosc dzialania zaznaczonych procesow
 */

namespace TaskManager
{
    public partial class MainWindow : Window
    {
        public TaskListVM TaskList { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            TaskList = new TaskListVM(procList);
            DataContext = TaskList;
            TaskList.userUpdate();
            //sorting
            procList.Items.SortDescriptions.Add(new SortDescription("ProcessName", ListSortDirection.Ascending));
            combobox.ItemsSource = Enum.GetValues(typeof(ProcessPriorityClass)).Cast<ProcessPriorityClass>();
            combobox.SelectedIndex = 0;
        }

        private void refresh(object sender, RoutedEventArgs e)
        {
            TaskList.userUpdate();
        }

        private void procList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TaskList.prev = TaskList.selected;
            var addedItems = e.AddedItems;

            if (addedItems.Count > 0)
            {
                Process selectedItem = (Process)addedItems[0];
                TaskList.selected = selectedItem;
            } else
            {
                TaskList.selected = null;
            }
            TaskList.updateChecked();
        }

        private void Kill(object sender, RoutedEventArgs e)
        {
            TaskList.Kill();
        }

        private void CloseMainWindow(object sender, RoutedEventArgs e)
        {
            TaskList.CloseMainWindow();
        }

        private void Change(object sender, RoutedEventArgs e)
        {
            TaskList.Chage((ProcessPriorityClass)combobox.SelectedItem);
        }

        private void checkboxEvent(object sender, RoutedEventArgs e)
        {
            TaskList.save();
        }
    }
}
