﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Threading;

namespace TaskManager
{
    public class TaskListVM : INotifyPropertyChanged
    {
        private Object updateLock = new Object();
        private List<Process> saved = new List<Process>();
        private Dictionary<int, ProcessModule> dict = new Dictionary<int, ProcessModule>();
        private ListBox list;

        public Process prev { get; set; } = null;
        public Process selected { get; set; } = null;
        public bool Checked { get; set; } = false;
        public ObservableCollection<Process> Processes { get; } = new ObservableCollection<Process>();

        public TaskListVM(ListBox list)
        {
            var timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(500) };
            timer.Tick += timerUpdate;
            timer.Start();
            this.list = list;
        }

        public void Kill()
        {
            if(selected != null)
            {
                ProcessModule mainModule = selected.MainModule;
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = mainModule.FileName;

                selected.Kill();
                Process.Start(startInfo);
            }
        }

        public void save()
        {
            if (selected != null)
            {
                var p = saved.Find(proc => proc.Id == selected.Id);
                if (p!=null)
                    saved.Remove(p);
                else
                {
                    saved.Add(selected);
                    dict[selected.Id] = selected.MainModule;
                }
            }
        }

        public void updateChecked()
        {
            Checked = false;
            
            if (selected != null)
            {
                var p = saved.Find(proc => proc.Id == selected.Id);
                if (p != null)
                    Checked = true;
            }
            OnPropertyChanged("Checked");
        }

        public void CloseMainWindow()
        {
            if (selected != null)
            {
                selected.CloseMainWindow();
            }
        }

        public void Chage(ProcessPriorityClass priority)
        {
            if (selected != null)
            {
                selected.PriorityClass = priority;
            }
        }

        public void userUpdate()
        {
            UpdateProcesses();
        }

        private void timerUpdate(object sender, EventArgs e)
        {
            UpdateProcesses();
        }

        private void UpdateProcesses()
        {
            lock (updateLock)
            {
                var currentIds = Processes.Select(p => p.Id).ToList();

                foreach (var p in Process.GetProcesses())
                {
                    if (!currentIds.Remove(p.Id))
                    {
                        Processes.Add(p);
                    } else if (selected != null && selected.Id == p.Id)
                    {
                        int index = Processes.IndexOf(Processes.First(proc => proc.Id == selected.Id));
                        Processes[index] = p;
                        list.SelectedItem = p;
                    }
                }

                foreach (var id in currentIds)
                {
                    var killed = Processes.First(p => p.Id == id);
                    Processes.Remove(killed);

                    if(dict.ContainsKey(id))
                    {
                        ProcessModule mainModule = dict[id];
                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.FileName = mainModule.FileName;

                        var oldProc = saved.Find(proc => proc.Id == id);
                        var newProc = Process.Start(startInfo);

                        saved.Remove(oldProc);
                        saved.Add(newProc);
                        dict[id] = newProc.MainModule;
                    }
                }

                OnPropertyChanged("Processes");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
